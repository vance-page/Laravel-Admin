<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_zh',20)->default('')->comment('中文名称');
            $table->string('name_en',50)->default('')->comment('英文名称');
            $table->string('abbreviation')->default('')->comment('简称');
            $table->string('continent',4)->default('')->comment('所属大洲');
            $table->string('area')->default('')->comment('面积');
            $table->string('capital')->default('')->comment('首都');
            $table->string('language')->default('')->comment('官方语言');
            $table->string('currency')->default('')->comment('货币');
            $table->string('system')->default('')->comment('政治体制');
            $table->string('area_code')->default('')->comment('国际电话区号');
            $table->string('domain_name')->default('')->comment('国际域名缩写');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
