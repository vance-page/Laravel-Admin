<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',20)->default('')->comment('账号');
            $table->string('password')->default('')->comment('密码');
            $table->string('email',60)->default('')->comment('邮箱');
            $table->char('phone',11)->default('')->comment('手机号码');
            $table->unsignedInteger('type')->default(0)->comment('类型');
            $table->string('website')->default('')->comment('网址');
            $table->string('webname')->default('')->comment('网址名称');
            $table->timestamp('reg_time')->nullable()->comment('注册时间');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
