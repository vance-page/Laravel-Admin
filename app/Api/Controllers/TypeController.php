<?php

namespace App\Api\Controllers;

use App\Models\Type;
use App\Http\Controllers\Controller;

class TypeController extends Controller
{
    /**
     *
     * @author Vance
     *
     * @return mixed
     */
    public function index()
    {
        return Type::get(['id', 'title as text']);
    }
}
