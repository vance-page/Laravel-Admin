<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $dates = ['reg_time'];

    /**
     *
     * @author Vance
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function types()
    {
        return $this->hasOne(Type::class,'id','type');
    }
}
