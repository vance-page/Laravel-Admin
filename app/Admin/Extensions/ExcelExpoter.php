<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;

class ExcelExpoter extends AbstractExporter
{
    /**
     * ExcelExpoter constructor.
     * @param Grid $title
     * @param $header
     * @param $rows
     */
    public function __construct($title, $header='Filename', $rows)
    {
        $this->title = $title;
        $this->header = $header;
        $this->rows = $rows;
    }

    /**
     *
     * @author Vance
     *
     */
    public function export()
    {

        Excel::create($this->title, function($excel) {
            // 设置标题
            $excel->setTitle($this->title);

            // 设置创作人
            $excel->setCreator('Vance')     // 作者
                ->setCompany('ShowData');   // 单位

            // 设置介绍说明
//            $excel->setDescription('一个测试的导出文件说明');

            $excel->sheet('Sheet1', function($sheet) {
                // 改变表样式
                $sheet->setStyle(array(
                    // 字体
                    'font' => array(
                        'name'      =>  'Microsoft YaHei UI',
                        'size'      =>  10,
                        'bold'      =>  false
                    )
                ));
                // 密码保护表(默认只读)
//                $sheet->protect('123456');

                // 表头
                $sheet->appendRow($this->header);
                // 这段逻辑是从表格数据中取出需要导出的字段
                $rows = collect($this->getData())->map(function ($item) {
                    return array_only($item, $this->rows);
                });

                $sheet->rows($rows);

            });

        })->export('xlsx');
    }
}