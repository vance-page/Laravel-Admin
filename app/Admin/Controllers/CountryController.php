<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ExcelExpoter;
use App\Models\Country;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CountryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('国家');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('国家');
            $content->description('编辑');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('国家');
            $content->description('创建');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Country::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->name_zh('中文名称');
            $grid->name_en('英文名称');
            $grid->abbreviation('简称');
            $grid->capital('首都');
            $grid->continent('所属大洲');
            $grid->system('政治体制');



            /**
             * TODO : 自定义筛选
             */
            $grid->filter(function($filter){

                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                // 在这里添加字段过滤器
                $filter->like('name_zh', '中文名称');
                $filter->like('continent', '所属大洲');
            });

            /**
             * TODO : 导出Excel
             */
            $title = '国家信息列表';
            $header = ['ID', '中文名称', '英文名称', '简称', '所属大洲'];
            $rows = ['id', 'name_zh', 'name_en', 'abbreviation', 'continent'];
            $grid->exporter(new ExcelExpoter($title, $header, $rows));

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Country::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name_zh', '中文名称')->rules('required');
            $form->text('name_en', '英文名称')->rules('required');
            $form->text('abbreviation', '简称');
            $form->text('continent', '所属大洲');
            $form->text('area', '面积');
            $form->text('capital', '首都');
            $form->text('language', '官方语言');
            $form->text('currency', '货币');
            $form->text('system', '政治体制');
            $form->text('area_code', '国际电话区号');
            $form->text('domain_name', '国际域名缩写');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
