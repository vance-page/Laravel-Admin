<?php

namespace App\Admin\Controllers;

use App\Models\Type;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Tree;
use Encore\Admin\Widgets\Box;
use Illuminate\Routing\Controller;

class TypeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('类型');
            $content->description('列表');

            $content->row(function (Row $row) {
                $row->column(6, $this->treeView()->render());

                $row->column(6, function (Column $column) {
                    $form = new \Encore\Admin\Widgets\Form();

                    $form->select('parent_id', '父级分类')->options(Type::selectOptions());
                    $form->text('title', '类型名称')->rules('required');
                    $form->number('sort', '排序');
                    $form->radio('status', '状态')->options(['1' => '正常', '0'=> '禁用'])->default('1');

                    $column->append((new Box(trans('admin.new'), $form))->style('success'));


                });
            });
        });
    }

    /**
     * Redirect to edit page.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect()->route('menu.edit', ['id' => $id]);
    }

    /**
     * @return \Encore\Admin\Tree
     */
    protected function treeView()
    {
        return Type::tree(function (Tree $tree) {
            $tree->disableCreate();

            $tree->branch(function ($branch) {
                $payload = "<strong>{$branch['title']}</strong>";

                if (!isset($branch['children'])) {
                    $payload .= "&nbsp;&nbsp;&nbsp;";
                }
                return $payload;
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param string $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('类型');
            $content->description('编辑');

            $content->row($this->form()->edit($id));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Type::form(function (Form $form) {
            $form->display('id', 'ID');

            $form->select('parent_id', '父级分类')->options(Type::selectOptions());
            $form->text('title', '类型名称')->rules('required');
            $form->number('sort', '排序');
            $form->radio('status', '状态')->options(['1' => '正常', '0'=> '禁用'])->default('1');

            $form->display('created_at', trans('admin.created_at'));
            $form->display('updated_at', trans('admin.updated_at'));
        });
    }
}
