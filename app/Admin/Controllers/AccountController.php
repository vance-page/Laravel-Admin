<?php

namespace App\Admin\Controllers;

use App\Models\Account;

use App\Models\Type;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AccountController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('账号');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('账号');
            $content->description('编辑');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('账号');
            $content->description('创建');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Account::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('types.title', '类型');
            $grid->title('账号');
            $grid->password('密码');
            $grid->email('邮箱');
            $grid->phone('手机号码');
            $grid->website('网址');
            $grid->webname('网站名称');
            $grid->reg_time('注册时间');

            /**
             * TODO : 自定义筛选
             */
            $grid->filter(function($filter){

                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                // 在这里添加字段过滤器
                $filter->like('website', '网址');
                $filter->like('webname', '网站名称');
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Account::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('title', '账号')->rules('required');
            $form->text('password', '密码')->rules('required');
            $form->email('email', '邮箱');
            $form->mobile('phone', '手机号码');
            $form->select('type', '类型')->options(Type::selectOptions());
            $form->url('website', '网址');
            $form->text('webname', '网站名称');
            $form->date('reg_time', '注册时间');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
